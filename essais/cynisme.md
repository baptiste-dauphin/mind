# Existence humaine
Notre existence n'a aucun sens. Notre vie n'a aucun sens. Après plus de 5000 années d'humanité nous ne sommes toujours pas capable de définir clairement ce qu'est la vie.
Nous ne sommes que des poussières d'étoiles agglomérées les unes aux autres en mouvement. On a décidé d'appeler ça "corps", "être vivant", ou "humain".
Nous n'avons aucune raisons d'exister. C'est le hasard du ballet des planètes, milliers d'année après milliers d'année, qui a fait qu'un jour nos molécules ce sont retrouvées au même endroit pour constituer notre corps.
Alors, on aime se rassurer. On se trouve des objectifs de vie, des projets, des raisons d'être. Mais en fin de compte tout ça n'existe que dans notre esprit intangible.

# Rapport au temps
Si on va vite c'est parce que notre monde est très grand, trop grand pour un être humain. Parce qu'il existe trop d'objets, de techniques, d'arts, de connaissances, d'être vivants. On pourra jamais tout rentrer dans notre esprit. Alors on fait des choix, on prend des décisions, on annule des moments où on aurait pu accumuler des connaissances. On se dit qu'il vaut mieux en savoir peu sur beaucoup que beaucoup sur peu. Alors on est devenu médiocre en tout. De manière relative, en proportion, de jour en jour on en sait de moins en moins.

# Artiste
Les plus grands génies, enfin ceux pour qui une masse critique de la population éprouve de l'amour et de l'admiration, ne font rien d'autre que d'imprimer dans le réel ce qui parait évident dans leur esprit.
Même Spinoza quand il a écrit "éthique" a marqué le monde des philosophes grâce au contraste entre le niveau de ses pensées et celui de ses lecteurs.
Qu'est ce qui définit la réussite, le succès ou encore le génie ? Rien de plus qu'une quantité minimale requise de la population éprouvant de l'admiration et de l'amour pour une production ?
Si on réalise que peu de personnes prennent le temps de s'adonner à la production d'art en en restant bloqué à l'état de spectateur. Et si on imagine la richesse globale de l'humanité dans le cas où chacun créerait et expérimenterait. Alors, ces "dit génies" se retrouveraient certainement noyé dans un océan de merveilles éclatantes et ne mettraient pas longtemps avant de retrouver l'anonymat, de descendre de leur piédestal et perdrait leur caractère exagérément divin.


## Draft
Mais maintenant que je suis plus âgé je me rend compte que depuis jeune j'ai toujours eu des pensées nihilistes !
Ben je dis que je me questione souvent sur le pourquoi du pourquoi et plus ca va et plus j en viens vite à l'idée que notre existence n'a aucun but. Et alors on ère. En se trouvant faussement des raisons d exister.
Tu vois Julien, si je n'avais pas trouvé la communication non violente et le langage de la vie interieur, y a des fois je pense que j'aurai des pensées suicidaire.
C'est bien la preuve qu'on ne peut pas prendre toutes nos decisions avec notre raison.
Quand j'ai des pensées nihilistes, je m'ecoute et apres quelques secondes ou minutes, je pense à quelque chose de bien terre à terre. Genre une meuf avec un super bon cul qui m'exciterait. Du coup ca me fait ressentir une emotion plus forte et ça me ratache à la vie ! Ahah
