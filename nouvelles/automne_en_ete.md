<!-- ![Walking people back sunset](../src/walking_person_back_sunset_man.jpg) -->

### Automne en été

Il est 19h30 le 29 août 2020. Je sors de chez le primeur. Je regagne la rue équipé de mon sac à dos. 45 minutes plus tôt j'étais encore dans la precipitation de me trouver une tenue pour sortir et ne pas arriver trop tard. Pourtant, une sérénité me gagne.

Aujourd'hui, je n'ai pas de tâche à accomplir, pas de réveil qui sonne, j'ai d'ailleurs fait une sieste sans mettre mon réveil avant de sortir de chez moi. Aucun ami qui m'attend au restaurant, au bowling, au cinema, en terrasse. Je n'ai pas non plus de petite copine qui m'attend à la maison. Personne ne m'appelle au téléphone. Je pourrais très bien partir maintenant ; prendre un train pour Barcelone, personne ne le saurait.

Bientôt je deviens un fantome, mais vivant et apaisé ! Je vois les autres mais eux ne me voient pas. L'espace de quelque instants je deviens l'invisible, l'omniscient. J'observe chaque passant que je croise et essaye d'imaginer sa vie, ses projets du moment, les raisons de sa présence dans ce doux froid à l'heure du souper. 

Je commence à silloner les rues du centre ville pour rentrer chez moi. Pour découvrir une nouvelle partie de la ville aujourd'hui, je me demande quel itinéraire je vais choisir. Je me remémore toutes les fois où je suis passé, sans y préter attention, dans cette même rue où je me tiens à cet instant. Etrangement aujourd'hui je ne ressens aucune nostalgie, simplement de l'émerveillement. Je constate les évènements, le passé et le présent tels qu'ils existent, sans jugement.

En cette fin de journée, la température n'est pas très élevée, il doit faire 15°C tout au plus. Nous avons beau être en été, j'aime porter cette veste dont, en temps normal, je me serais passé. Certains arbres ont des feuilles qui commencent déjà à jaunir. Le ciel non plus n'est pas ce qu'on pourrait qualifier de saison. A moitié nuageux, à moitié sombre. Aujourd'hui il a décidé de se montrer tel qu'il est en Normandie. Ce n'est pas un ciel de carte postale. Mais c'est un ciel qui rassure par sa sincérité. S'il décidait de me pleuvoir dessus, je l'accepterais volontiers.

Voilà que je commence à sentir une odeur de marron chaud. Je sais qu'à cette période de l'année il n'y aucun vendeur ambulant dans les rues de Rouen. Cependant, par mes envies, volontiers, je me laisse berner. Ne dit-on pas que la douleur est physique mais que la souffrance dans la tête ? Je pense qu'il en est de même pour les odeurs. 

Personne ne m'attend alors !? Est-ce possible ? Je crois bien que oui.  
Pour toutes ces fois où j'ai rêvé qu'on me laisse tranquile. Toutes ces fois où j'aurais payé cher pour mettre l'écoulement du temps sur pause. Ca y est, enfin j'y suis. L'aisance, la confiance et la liberté me gagnent.

Tout d'un coup, rempli d'énergie, je prends conscience de cette dernière. Comme si elle avait toujours été présente, mais endormie. Ces dernières minutes je n'ai avalé aucunes potions magiques et pourtant je me sens plus fort qu'avant ! Désormais cette énergie me parait vitale. Jamais plus je ne veux qu'elle me quitte.

Mais, comment vivais-je auparavant ? Quand je n'y prêtais aucune attention... Peut-être qu'à moitié.  
Une chose est sûre, je veux continuer de vivre des moments de grands frissons.
