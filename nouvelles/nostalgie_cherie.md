### Nostalgie chérie :kiss:

#### Moment de nostalgie : 
- [Need For Speed Underground 2 Soundtrack (Continuous Mix)](https://www.youtube.com/watch?v=qPRU8jbZemk)  
T'as 15 ans, c'est vendredi soir, t'es tout seul dans ta chambre. Tu viens de lancer Need For Speed underground 2. Tout est sombre, y a que ton écran qui t'eclair le visage. Tu conduis ta caisse sur l'autoroute. T'es à fond. Il pleut. T'entends "blakc betty" version metal qui commence. Là : T'actives la nitro. Nostalgie
La generation fortnite ne comprendra jamais ces emotions vecues pendant une soirée comme celle la.
C'est etrange de se dire que ce jeu est sorti fin 2004 y a 16 ans. Ca me rappelle même des moments encore plus lointains. Underground 1 était sorti 1 an plus tôt et ça me parait être le temps de toute une vie. Ce qui est pas faux en soit.

- To be continued :)



#### Paroles directes

J'viens d'faire un festival. Tellement nostalgique un mois après, en soirée entourrée de tous mes potes, je chiale déjà en y repensant.

Ah putin la nuit j'en ai cramé de l'essence, à revenir roder dans le quartier de mon adolescence.

Tellement nostalgique, que je regrette des moments que j'ai même pas vécu. Comme en été 85 quand mon beau pere directeur de colonie planait pendant sa sieste en ecoutant le dernier vinyl de Pink FLoyd, ca lui faisait l'effet d'un gros join. Ensuite il emenaient toute la colonie avec un minibus à la plage sauvage pour passer l'apres midi au soleil à faire du surf. Ces moments me manquent alors que putin j'étais pas né !

Ma nostalgie c'est comme ma copine. Quand je m'entends pas et que je me la met à dos et que je la repousse elle me pompe toute mon energie, j'ai aucunes confiances en moi. Mais si je l'accepte et que je m'ouvre pleinement à elle, comme une partie intégrante de ma personalité, elle est capable de me donner des ailes. J'ai l'impression que je peux tout faire, tout entendreprendre. D'ailleurs au bout d'un moment elle me fait du bien. Tellement que c'est comme si elle disparaissait. Avec le temps j'apprends à la connaitre, c'est ma meilleur amie, mon plan cul. De plus en plus on se fait des calins. Peut etre qu'on se marira un jour.
Elle m'aide à me reconnecter et faire la paix avec mon passé. Y a qu'elle qui sait me rappeler à quel point j'ai vécu des moments aussi beaux et puissants. Surtout dans les moments où ma __modestie__ me minimise devant tout le monde, ma nostalgie est là pour me rappeler toutes les batailles que j'ai remportées :

*list des moments de complicités fortes*  : 

- Les moments de complicités absolu au collège à la récré avec Guillaume, Vali et Flo. Les barres de rire pendant les repet de la pièce de théâtre en Latin. Je savais même pas ce que je repétais.
- Les cours de latin où on passait 2h à se taper des barres non-stop. Je suivais rien en cours, mais c'est pas grave je demandais à Elise si je pouvais copier sur elle. Je lui plaisait, du coup elle acceptait.
- Les courses poursuite à 300 sur l'autoroute contre le boss final de Need for Speed
- Le premier Rathalos sur Monster Hunter
- Les dizaines d'heures la nuit chez Guillaume à farmer les quêtes sur Monster Hunter
- Les centaines heures passé à Farmer le LOD sur nostale avec la famille Expérience.
- Les centaines d'heures en co-op ou en multi sur Call of Duty Modern Warfare 2. A construire des oeilleres en carton pour pas que Florian puisse tricher en regardant sur l'écrans des autres.
- La fois où ma mère a bien voulu m'acheter GTA Vice City
- Le jour ou j'ai eu mon premier abonné à ma chaine youtube de tutorial sur les diabolos.
- La première fois ou j'ai joué de la guitare sur un ampli.
- La première fois où je suis rentré dans une salle d'escalade.
- La première fois ou j'ai embrassé une fille.
- La première fois qu'un fille m'a sucé.



