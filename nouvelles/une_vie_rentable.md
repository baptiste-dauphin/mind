Champ lexical : Rentabilité, temps, précipitation, charge de travail, harassement, etouffement/sufocation, répétition, degradation personnelle, profusion, insatisfaction, auto-critique, jugement, manque d'attention, manque d'amour, mise en retrait, crédulité.
Âme du texte : Manque de conscience et d'amour propre + grosse fierté

### Une vie rentable ?

Il est 19h00, après une énième journée de travail, je rentre enfin dans mon appartement et me défait de mon lourd sac à dos. Je suis à peine fiers de ma journée pourtant j'y ai consacré toute mon énergie. Avec 15 minutes de pause au milieu de l'après-midi c'est à peine si j'ai eu le temps de respirer avec les 3 projets sur lesquels je travail en même temps. Ce n'est pas grave. Pas besoin d'oxygen, quand on aime on ne compte pas.
La seule maigre satisfaction que j'en tir est d'avoir réussi à optimisé ma journée entre auto-formation et travail rendu à mon entreprise.

Je me presse de faire à manger avec ce qui me tombe sous la main. Je n'ai plus de carottes ni de tomates ? Ce n'est pas grave, je peux très bien manger des pommes de terre pour la troisieme fois consecutive. 

Je veux tellement tout rentabiliser que je me sens coupable de ne pas noter chaque pensée qui me passe par la tête. Et oui, elle pourrait agrémenter un récit, une nouvelle ou un essai. Ecris que tout le monde trouverait forcément génial et qui me propulserait sous le feu des projecteur. Si on pouvait me regarder ce serait génial. Sinon je pourrais simplement montrer mes écrit et mes medailles aux filles qui me plaisent, ça m'aiderait surement à attirer leur attention et à coucher avec elles.

- Aller à la salle de sport
- Poursuivre l'écriture de mes essais/nouvelles
- Apprendre un nouveau morceau de Bach au piano.
- Poursuivre mes cours de solfege
- Jouer à TrackMania, je l'ai acheté 45€ il y a 2 semaines, je n'y ai pas encore assez joué. 
- Aller boire un verre avec cet ami que je n'ai pas vu depuis 1 an. En plus il me permetra très certainement de m'inviter à des soirées où je rencontrerais de futur client.
- Poursuivre ma lecture des essais de Montaigne pour me cultiver. 
- Revoir mon profil Tinder, je peux certainement l'améliorer pour avoir plus de chance de faire des rencontres.

#### Fiertée 
J'aimerais m'elever et vivre une vie riche et forte en sensation. Faire des choses incroyable. Je deteste la banalité, je fais le classique. Je suis addicte à l'originale. J'essaye de m'extraire des activités trivialles, je ne regarde plus la base de la pyramide de maslow. Sortir avec une meuf ? Pitié quelle perte de temps et d'énergie, à moins de sortir avec une déesse, je n'en vois pas ce que cela pourrait m'apporter. Faire une balade en forêt !? Flanner avec un ami dans les rues de ma ville sans but ? Très peu pour moi. Je regarde toujours plus haut. 