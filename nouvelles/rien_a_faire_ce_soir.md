Il est 21h30, je sais que j'ai encore 2h30 devant moi, avant qu'il soit minuit.
J'ai du temps à dépenser. Mais j'ai envie de rien.
J'ai tout j'ai mais j'ai envie de rien. J'ai matter des vidéos pour savoir comment rien faire. J'ai matté des videos avec un mauvais marketing qui disait d'eteindre son ordinateur maintenant.
Vas-y pour une fois je pourrais volontairement ne rien faire et me prendre pour un moine boudhiste. Sans m'en rendre compte je me tape encore un délire de riche.
Mais comment on fait pour rien faire ?
Les jeux videos, jouer du piano, essayer de composer de la musique, lire un livre, appeler quelqu'un, aller sur tinder, YouTube, ecrire, matter un film, planifier mes prochaines sorties.
Tout ça c'est faire des trucs. C'est plus fort que moi faut qu'fasse un truc, sinon j'aurai perdu ma soirée. J'ai déjà que 2 heures de temps libre par soir, c'est pas pour rien faire.
Est ce que faire de la meditation c'est faire quelque chose ?

# moment que j'estime avoir vecu inutilement
Pour toutes ces heures que j'ai remplis avec du vide.
Pour toutes les fois où je suis resté beaucoup trop tard après les séances d'escalade.
Ces soirées nocturnes à tourner dans ma ville en me prennant pour le parain.
Pour toutes ces soirées passé sur le net à regarder des vidéos dont je me souvenais même plus la semaine d'après. Auxquels j'ai jamais repensé, qui m'ont rien appris. J'ai même pas ris. Quand j'ai à peine souris en regardant des compil pétés de Russes qui dansent torchés. 
Pour toutes ces soirées dans mon ancienne vie à passer 5 soirs par semaine chez des gens que je qualifierai à peine de connaissance.
Pour toutes ces soirées passées à esquiver ma meuf. A esperer trouver enfin la liberté dans chaque clope qu'on me tendait. Alors qu'elle m'attendait toute sage, en ambuscade, magnifiquement apprêtée.
Pour toutes ces soirées entières données à ma nostalgie à matter des compils de Need for Speed ou GTA de ma jeunesse, alors que quand j'y étais, au bout de 3 mois j'étais lassé. 

Quand je passais plus de temps à discuter apres que pendant la seance.
