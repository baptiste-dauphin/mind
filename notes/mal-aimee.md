# Ma mal-aimée

<!-- Mes rapport de force avec toi -->
Toi que j'ai tant essayé d'ignorer.  
Toi que j'ai essayé de faire disparaître.  
Toi contre qui j'ai perdu tous mes combats.  
Je ne compte plus ni l'énergie ni le temps consacré à nos rapports de force.  


<!-- Tous les moments où tu m'as fais du bien (malgrès tout) -->

Toi qui depuis 2 ans devient doucement amicale.  Tu me fais de moins en moins peur.  Je commence même à t'apprécier.  
Je réalise, que pour moi, tu es un guide, un cap fixe et fiable sur lequel je peux toujours compter. Constante à travers le temps.  
Lorsque je te regarde droit dans les yeux et que je t'écoute vraiment, tu m'indiques la direction exacte. 
Par ta seule nature de mal-aimée et pourtant bien présente depuis ma naissance, tu me persuades de t'écouter quand je veux être moi-même.
Avec le temps, je prends plaisir à perdre mes combats contre toi. D'ailleurs ils commencent à se faire rares.  
Maintenant je préfère t'inviter à ma table. Le plus petit des câlins est 100 fois plus beau que le plus épique des combats.


<!-- Si tu n'existais pas  -->
Pour rien au monde je voudrais ne plus avoir peur en me séparant de toi.  
Comment savoir si une chose est bonne ou mauvaise pour moi ? Dans quelle direction aller ?  
Je voguerais au hasard des courants que je ne maitrise pas dans un océan de doutes et des questions sans réponses.  
Oh, cela ne me donne aucune envie, rien que d'imaginer cette situation me donne froid dans le dos. Le temps d'une pensée, même sans exister, tu brilles par ton absence ; tu transpires la nécessite. Encore une fois, j'aime ta présence, j'ai besoin de toi.


Ô ma peur.  Veux-tu bien rester à mes côtés.  
Acceptes-tu de continuer de vivre avec moi ?
