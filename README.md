## [Nouvelles](nouvelles)
- [Automne en été](nouvelles/automne_en_ete.md)

## [Album](paroles)
- Illusions
- Vie trop confortable
- J'ai pas le temps (d'écrire)
- L'imposteur
- Pourquoi moi ? (à découper par thème)
- Banal
- Viol de cerveau (tout va trop vite)
- Égo sur-dimenssionné
- Sans moi
- Ce que j'aime
- Sensible
- Margaux
- Schizophréne
- Le gentil
- [Nostalgie chérie :kiss:](nouvelles/nostalgie_cherie.md)

### Old good writings
[Backup google keep : Mes vérités, à quoi la vie m'appelle ? Mes adages](archives/backup_google_keep.md)

## Définitions
### [Essai](https://fr.wikipedia.org/wiki/Essai)  
Oeuvre de réflexion portant sur les sujets les plus divers et exposée de __manière personnelle__, voire subjective par l'auteur.  
Peut être polémique ou partisan.  
C'est un texte littéraire qui se prête bien à la __réflexion philosophique__.  
Le terme « essai » est dérivé du latin exagium, « __juger, examiner, peser__ ».


### [Nouvelle](https://fr.wikipedia.org/wiki/Nouvelle)  
Une nouvelle est un __récit habituellement court__. Apparu à la fin du Moyen Âge, ce genre littéraire était alors proche du roman et __d'inspiration réaliste__, se distinguant peu du conte.  
À partir du xixe siècle, les auteurs ont progressivement développé d'autres possibilités du genre, en s'appuyant sur la concentration de l'histoire pour renforcer l'effet de celle-ci sur le lecteur, par exemple par une chute surprenante.  
Les thèmes se sont également élargis : la nouvelle est devenue une forme privilégiée de la littérature fantastique, policière, et de science-fiction.


### [Émotion](https://fr.wikipedia.org/wiki/%C3%89motion)  
Expérience psychophysiologique complexe et intense (avec un __début brutal et une durée relativement brève__) de l'état d'esprit d'un individu animal1 liée à un objet repérable lorsqu'il réagit aux influences biochimiques (internes) et environnementales (externes).  
Chez les humains, l'émotion inclut fondamentalement __un comportement physiologique, des comportements expressifs et une conscience__.  
L'émotion est associée à l'humeur, au tempérament, à la personnalité et à la disposition et à la motivation.

Le mot « émotion » provient du mot français « émouvoir ». Il est basé sur le latin emovere, dont e- (variante de ex-) signifie « hors de » et movere signifie « mouvement »3. Le terme lié « motivation » est également dérivé du mot movere.

### Le style

>Travailler le style, c'est pinailler sur les mots.

>Non ! Le style c'est se donner les moyens de rendre les idées `mémorables`.  

Lors de la preparation d'un discours, presentation, billet ou vidéo, prendre le temps d'identifier les 2,3 idées principales.  
Une fois qu'on les a, faire l'effort de les reformuler en utilisant des `figures de style`.  
Car une figure de style, c'est un `véhicule conçu pour aller se garer tout droit dans la mémoire de votre public`.

Il existe 
- `Figures de discours` capter l'attention du public en rompant avec la monotonie et la médiocrité du discours quotidien. Travail sur le `rythme`, `répétition`, `sonorité`, `parallelisme`.
- `Figures de penser` rendent les idées plus préhenssibles par le public, en les traduisant par quelque chose plus `visuel`, plus `connu`. Ex: `Metaphore`, `analogie`. 
