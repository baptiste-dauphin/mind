__Âme du morceau__ : 
- L'envie de prendre soin des autres, de la communauté, des générations futures sans rien attendre en retour.  
- N'attendre aucun signal pour agir.  
- Ne recevoir aucunes validations, aucun remerciement pour les montagnes déplacées.  
- Histoire d'un bien-faiteurs/super-hero inconnu comme il en existe des milliers.
- Ode à la vaillance et à la philantropie.
- Personnage mi homme mi dieu/nature/visionnaire.

__Idées de contraintes__ : 
- Nothing for now

__Champ lexical__ : 
- Force
- Labeur
- Anticipation
- Sagesse
- Bravour
- Spiritualité. 

___
___

To be written :)