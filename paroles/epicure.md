__Âme du morceau__ : L'envie de profiter des plaisirs simples de la vie 
__vs__ un homme moderne sans conscience (hyper mal) qui cherche à s'enrichir et accumuler tout à n'importe quel prix sans savoir  pourquoi.

__Idées de contraintes__ : 
- Discours à la premiere personne
- Langage soutenu
- Aucunes insultes (ou alors bien caché)
- Exposer ma faiblesse

__Champ lexical__ : Violence, travail, temps, force. 
___
___

Tu me regardes, je te construit une tour de garde.  
Tu me tourne le dos je commence à me construire un bateau.  
Je sais que tu n'aimes pas ça.  
Tu voudrais plus.     

Je sais que j'ai du géni.  
Mais je ne veux pas te donner mon énergie.  
C'est pourtant dans tes mains que j'ai placé ma sécurité.  
Et grâce à toi que j'ai mon bel appartement.  

Pourquoi suis-je coincer dans cet enveloppe !?  
Je tuerai pour avoir plusieurs vies.  
Mon calvere c'est d'avoir compris qu'on peut tout faire.  
Apprendre que les journées font 24 heures m'a anéanti.   

De mon envie je remplirai aisément 10 vies  
Avec mes muscles tu crois que je suis un homme mais je ne suis qu'envie.  

Tu me maintien juste au dessus de l'esclavage.  
Comment je puis-je te respecter ?  
...
