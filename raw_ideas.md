- La vie c'est que des rencontres, des croisements, des entremelement puis des separations. 

- Il suffit qu'une toute petite chose commence à me faire sortir de ma zone de confort pour que j'extra-extrapole et que soit aspiré dans une phase d'euphorie. A ce moment je serais capable de suivre n'importe qui tant que c'est pour quelque chose de nouveau.

- Tant que j'ai une bonne relation avec quelqu'un, je pourrais faire n'importe quoi avec elle.

- La magie c'est ni plus ni moins que quand tu comprends pas.

- J'adore faire ma vaisselle moi même. C'est là où j'ai mes plus grandes inspirations. Comme si faire une tâche ultra basique laissait mon cerveau respirer.


- "Qu'est ce qui te plaît le plus comme catégories de sujets pour écrire ?"  
Oh la. Je pense que j'ai une tendance à être hyper-actif par curiosité.
Ce que j'affectionne c'est : 
Le cynisme, l'émerveillement,  la peur,  la responsabilité, la fragilité, le rapport au temps, le respect, l'observation, l'amusement, la douceur, la précision, les interactions.

- Curieusement, quand je bad et que j'écoute une musique badante, ça va mieux

- Une entreprise n'est pas necessairement une société déclarée au registre du commerce. En ce sens, nous sommes tous, à minima, chef de l'entreprise de notre propre existance. Simplement nous ne cherchons pas à maximiser l'argent mais le bonheur. Nous essayons de réaliser nos projets de vie. De manière clairement défini ou non, nous avons des envies et des objectifs que nous aimerions atteindre. Chaque semaines nous expérimentons des déconvenu. Nous sommes amené à recentrer nos actions sur nos envies profondes.
